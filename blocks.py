from pygame import *

PLATFORM_WIDHT = 32
PLATFORM_HEIGHT = 32
PLATFORM_COLOR = 'BLACK'


class Platform(sprite.Sprite):
	def __init__(self, x, y):
		sprite.Sprite.__init__(self)
		self.image = Surface((PLATFORM_HEIGHT, PLATFORM_WIDHT))
		self.image.fill(Color(PLATFORM_COLOR))
		self.rect = Rect(x, y, PLATFORM_WIDHT, PLATFORM_HEIGHT)
