import pygame
from pygame import *
from sprit import *
from blocks import *


WIN_WIDTH = 800
WIN_WEIGHT = 600
DISPLAY = (WIN_WIDTH, WIN_WEIGHT)
BACKGROUND_COLOR = 'YELLOW'
PLATFORM_HEIGHT = 32
PLATFORM_WIDTH = 32
PLATFORM_COLOR = 'BLACK'



def main():
	hero = Player (55,55)
	left = right = up = 0
	entities = sprite.Group()
	entities.add(hero)
	platforms = []
	
	level = [
		"-------------------------",
		"-                       -",
		"-                       -",
		"-            --         -",
		"-                       -",
		"--                      -",
		"-                       -",
		"-                   --- -",
		"-                       -",
		"-                       -",
		"-      ---              -",
		"-                       -",
		"-   -----------         -",
		"-                       -",
		"-                -      -",
		"-                   --  -",
		"-                       -",
		"-                       -",
		"-------------------------"]
	timer = pygame.time.Clock()
	
	
	screen = pygame.display.set_mode(DISPLAY)
	pygame.display.set_caption('')
	bg = Surface((WIN_WIDTH, WIN_WEIGHT))
	bg.fill(Color(BACKGROUND_COLOR))
	
	while True:
		timer.tick(90)
		for e in pygame.event.get():
			if e.type == pygame.QUIT:
				raise (SystemExit, 'QUIT')
			if e.type == pygame.KEYDOWN and e.key == pygame.K_LEFT:
				left = True
			if e.type == pygame.KEYDOWN and e.key == pygame.K_RIGHT:
				right = True
			
			if e.type == pygame.KEYUP and e.key == pygame.K_RIGHT:
				right = False
			if e.type == pygame.KEYUP and e.key == pygame.K_LEFT:
				left = False
			if e.type == pygame.KEYDOWN and e.key == pygame.K_UP:
				up = True
			if e.type == pygame.KEYUP and e.key == pygame.K_UP:
				up = False
		screen.blit(bg, (0, 0))
		x=y=0
		
		for row in level:
			for col in row:
				if col == '-':
					pf = Platform(x, y)
					entities.add(pf)
					platforms.append(pf)
				x+= PLATFORM_WIDTH
			y+= PLATFORM_HEIGHT
			x = 0
		hero.update(left, right, up, platforms)
		entities.draw(screen)
		pygame.display.update()
if __name__ == '__main__':
	main()
	
